console.log("hello");


console.log(document);
// result: document HTML code

const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName); // always check your target by using console.log

const txtLastName = document.querySelector("#txt-last-name");



// result: input field / tag

/*
	alternative ways:
		>> document.getElementById("txt-first-name");
		>> document.getElementByClassName("text-class");
		>> document.getElementByTagName("h1");
*/

// target the full name

let spanFullName = document.querySelector("#span-full-name");
//console.log(spanFullName);

//Event listeners

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
});

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})

// stretch
const keyCodeEvent = (e) => {
	let kc = e.keyCode;
	if (kc === 65){
		e.target.value = null;
		alert('Someone cicked a');
	}
}

txtFirstName.addEventListener('keyup', keyCodeEvent);


// Activity solution

const txtFullName = (event) => {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
}

txtFirstName.addEventListener('keyup',txtFullName) + txtLastName.addEventListener('keyup', txtFullName)